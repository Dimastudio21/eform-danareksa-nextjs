module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backroundImage: theme=>({
        'image': "url('../src/assets/background-biru.jpg')"
      }),
      colors : {
        blue : {
          450: '#074c96',
          451: '#007bff'
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
