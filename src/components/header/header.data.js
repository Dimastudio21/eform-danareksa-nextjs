export default [
  {
    path: 'beranda',
    label: 'Beranda',
  },
  {
    path: 'registrasi',
    label: 'Registrasi',
  },
  {
    path: 'danareksaonline',
    label: 'Danareksa Online',
  },
  {
    path: 'hubungikami',
    label: 'Hubungi Kami',
  },
];
