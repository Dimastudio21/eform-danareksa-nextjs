import Ojk from 'assets/mitra/ojk.png';
import Idclear from 'assets/mitra/idclear.png';
import Idx from 'assets/mitra/idx.png';

export default {
  widgets: [
    {
      id: 1,
      iconSrc: Ojk,
      altText: 'Idclear',
      title: 'Otoritas Jasa Keungan',
      description:
        '',
    },
    {
      id: 2,
      iconSrc: Idclear,
      altText: 'Chat',
      title: 'ID Clear',
      description:
        '',
    },
    {
      id: 3,
      iconSrc: Idx,
      altText: 'Github',
      title: 'Idx',
      description:
        '',
    },
  ],
  menuItem: [
    {
      path: '/',
      label: 'Home',
    },
    {
      path: '/',
      label: 'Adversite',
    },
    {
      path: '/',
      label: 'Supports',
    },
    {
      path: '/',
      label: 'Marketing',
    },
    {
      path: '/',
      label: 'Contact',
    },
  ],
};
