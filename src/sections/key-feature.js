/** @jsx jsx */
import { jsx } from 'theme-ui';
import { Container, Grid } from 'theme-ui';
import SectionHeader from '../components/section-header';
import FeatureCardColumn from 'components/feature-card-column.js';
import Performance from 'assets/no-1.png';
import Partnership from 'assets/no-2.png';
import Subscription from 'assets/no-3.png';
import Support from 'assets/no-4.png';

const data = [
  {
    id: 1,
    imgSrc: Performance,
    altText: 'Siapkan Dokumen dan Isi Form',
    title: 'Siapkan Dokumen dan Isi Form',
    text:
      'Siapkan e-KTP, NPWP, dan Rekening Tabungan BRI* Anda. Lalu isi form sesuai dengan data diri dan menyetujui syarat dan ketentuan.',
  },
  {
    id: 2,
    imgSrc: Partnership,
    altText: 'E-mail Konfirmasi',
    title: 'E-mail Konfirmasi',
    text:
      'E-mail Konfirmasi',
  },
  {
    id: 3,
    imgSrc: Subscription,
    altText: 'Top Up Dana',
    title: 'Top Up Dana',
    text:
      'Anda bisa melakukan pengisian saldo ke RDN BRI Anda minimal Rp 1 Juta.',
  },
  {
    id: 4,
    imgSrc: Support,
    altText: 'Selamat Berinvestasi',
    title: 'Selamat Berinvestasi',
    text:
      'Akun Anda sudah dapat digunakan. Nikmati kemudahan berinvestasi dengan aplikasi BRI Danareksa.',
  },
];

export default function KeyFeature() {
  return (
    <section sx={{ variant: 'section.keyFeature' }} id="feature">
      <Container>
        <SectionHeader
          slogan="Persiapan Registrasi"
          title="E-Form BRI Danareksa Sekuritas"
        />

        <Grid sx={styles.grid}>
          {data.map((item) => (
            <FeatureCardColumn
              key={item.id}
              src={item.imgSrc}
              alt={item.altText}
              title={item.title}
              text={item.text}
            />
          ))}
        </Grid>
      </Container>
    </section>
  );
}

const styles = {
  grid: {
    width: ['100%', '80%', '100%'],
    mx: 'auto',
    gridGap: [
      '35px 0',
      null,
      '40px 40px',
      '50px 60px',
      '30px',
      '50px 40px',
      '55px 90px',
    ],
    gridTemplateColumns: [
      'repeat(1,1fr)',
      null,
      'repeat(2,1fr)',
      null,
      'repeat(4,1fr)',
    ],
  },
};
