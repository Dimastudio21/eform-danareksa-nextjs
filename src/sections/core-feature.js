/** @jsx jsx */
import { jsx, Container, Box } from 'theme-ui';
import TextFeature from 'components/text-feature';
import Image from 'components/image';

import FeatureThumb from 'assets/macbook.png';
import shapePattern from 'assets/shape-pattern2.png';

const data = {
  subTitle: 'bri danareksa sekuritas',
  title: 'BRI DANAREKSA SEKURITAS',
  description:
    'PT Danareksa Sekuritas didirikan pada tanggal 1 Juli 1992 sebagai pioneer perusahaan jasa keuangan yang bergerak di bidang pasar modal. Pada akhir Desember 2018 Danareksa Sekuritas resmi menjadi anak perusahan PT Bank Rakyat Indonesia dengan kepemilikan saham 67% dan sisanya 33% oleh PT Danareksa (persero), serta resmi berganti nama menjadi PT BRI Danareksa Sekuritas pada Oktober 2020. Sebagai perusahaan yang telah terdaftar pada Otoritas Jasa Keuangan (OJK), BRI Danareksa Sekuritas memiliki ijin sebagai jasa penasehat keuangan.',
  btnName: 'Get Started',
  btnURL: '#',
};

export default function CoreFeature() {
  return (
    <section sx={{ variant: 'section.coreFeature' }}>
      <Container sx={styles.containerBox}>
        <Box sx={styles.contentBox}>
          <TextFeature
            subTitle={data.subTitle}
            title={data.title}
            description={data.description}
            btnName={data.btnName}
            btnURL={data.btnURL}
          />
        </Box>
        <Box sx={styles.thumbnail}>
          <Image src={FeatureThumb} alt="Thumbnail" />
          <Box sx={styles.shapeBox}>
            <Image src={shapePattern} alt="Shape" />
          </Box>
        </Box>
      </Container>
    </section>
  );
}

const styles = {
  containerBox: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexWrap: ['wrap', null, null, 'nowrap'],
    pb: [0, 7, 0, null, 7],
  },
  contentBox: {
    flexShrink: 0,
    px: [0, null, '30px', 0],
    textAlign: ['center', null, null, 'left'],
    width: ['100%', '80%', null, 340, 400, 430, null, 485],
    pb: ['50px', '60px', null, 0],
    mx: ['auto', null, null, 0],
    '.description': {
      pr: [0, null, 6, 7, 6],
    },
  },
  thumbnail: {
    display: 'inline-flex',
    position: 'relative',
    mr: 'auto',
    ml: ['auto', null, null, null, 7],
    '> img': {
      position: 'relative',
      zIndex: 1,
      height: [310, 'auto'],
    },
  },
  shapeBox: {
    position: 'absolute',
    bottom: -65,
    right: -165,
    zIndex: -1,
    display: ['none', 'inline-block', 'none', null, 'inline-block'],
  },
};
