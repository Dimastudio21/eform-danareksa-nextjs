import { useEffect } from 'react';
import Router from 'next/router';
import { initGA, logPageView } from 'analytics';
import 'react-multi-carousel/lib/styles.css';
import 'react-modal-video/css/modal-video.min.css';
import 'rc-drawer/assets/index.css';
import 'typeface-dm-sans';
import { ChakraProvider } from '@chakra-ui/react';
import 'tailwindcss/tailwind.css'

export default function CustomApp({ Component, pageProps }) {
 return (
    <ChakraProvider>
      <Component {...pageProps} />
    </ChakraProvider>
  );  
}
