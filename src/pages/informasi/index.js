import { jsx } from 'theme-ui';
import { Container, Box, Heading, Text, Image, Button } from 'theme-ui';
import Header from "components/header/header"
import Layout from "components/layout"
import { StickyProvider } from "contexts/app/app.provider"
import { ThemeProvider } from 'theme-ui';
import theme from 'theme';
import { useState } from 'react';
import { useForm } from "react-hook-form";
import { useRouter } from 'next/router'

export default function Informasi() {
    const router = useRouter()
    const [getImgaeLeft, setImageLeft] = useState(null);
    const [getImgaeRight, setImageRight] = useState(null);
    const [isDeletePhotoLeft, setIsDeletePhotoLeft] = useState(false);
    const [isDeletePhotoRight, setIsDeleteRight ] = useState(false);
    
    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const onSubmit = data => router.push("/informasidua")
   


    const handleChangeImageLeft = (e) => {
        if (URL.createObjectURL(e.target.files[0]) !== null) {
            let imageLeft = URL.createObjectURL(e.target.files[0]);
            setImageLeft(imageLeft);
            setIsDeletePhotoLeft(true);
        }
    }

    const handleChangeImageRight = (e) => {
        if (URL.createObjectURL(e.target.files[0]) !== null) {
            let imageRight = URL.createObjectURL(e.target.files[0]);
            setImageRight(imageRight);
            setIsDeleteRight(true);
        }
    }

    const deletePhotoLeft = () => {
          setImageLeft(null);
          setIsDeletePhotoLeft(false);
    }

    const deletePhotoRight = () => {
        setImageRight(null);
        setisDeletePhotoRight(false);
  }

    return (
        <ThemeProvider theme={theme}>
            <StickyProvider>
                <Layout>
                    <Container sx={styles.container}>
                        
                        <div className="border-solid  mx-auto bg-white overflow-hidden mt-40 mr-10 ml-10 flex flex-wrap items-center justify-center">
                        <div class="rounded-full h-24 w-24 flex items-center justify-center bg-blue-450 mr-20 text-white">1</div>
                        <div class="rounded-full h-24 w-24 flex items-center justify-center bg-gray-200 mr-20 text-white">2</div>
                        <div class="rounded-full h-24 w-24 flex items-center justify-center bg-gray-200 mr-20 text-white">3</div>
                        <div class="rounded-full h-24 w-24 flex items-center justify-center bg-gray-200 mr-20 text-white">4</div>

                        </div>
                        <div className="border-solid  mx-auto bg-white overflow-hidden border-2 mt-20 mr-10 ml-10 shadow-2xl">
                            <div className="md:flex bg-blue-450 m-8 rounded-2xl">
                                <div style={styles.card}>
                                    <p style={styles.title}>INFORMASI DASAR</p>
                                </div>
                            </div>

                            <form onSubmit={handleSubmit(onSubmit)} className="ml-8 mr-8">
                                {/* <div className="flex flex-wrap -mx-3 mb-6">
                                    <div className="w-full md:w-1/2 px-3">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
                                           Branch ID
                                        </label>
                                        <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" 
                                        {...register("branchId", { required: true })} 
                                        />
                                        {errors.branchId && <span><p style={{color:"red"}}>This field is required</p></span>}
                                    </div>
                                    <div className="w-full md:w-1/2 px-3">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-last-name">
                                            Referal Code
                                        </label>
                                        <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" 
                                        {...register("referalCode", { required: true })} 
                                        />
                                        {errors.referalCode && <span><p style={{color:"red"}}>This field is required</p></span>}
                                    </div>
                                </div> */}
                                <div className="flex flex-wrap -mx-3 mb-2">
                                    <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state">
                                        Branch ID
                                        </label>
                                        <div className="relative"> 
                                        <select className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        {...register("branchId")}>
                                            <option value=""></option>
                                            <option value="B8393">B8393</option>
                                            <option value="B8355">B8355</option>
                                        </select>
                                        {errors.branchId && <span><p style={{color:"red"}}>This field is required</p></span>}
                                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-city">
                                            Referal Code
                                        </label>
                                        <input className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-city" type="text"/>
                                    </div>
                                    <div className="w-full md:w-1/3 px-3 mb-6 md:mb-0">
                                        <label className="block tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-state">
                                        Jenis rekening yang anda inginkan ?
                                        </label>
                                        <div className="relative">
                                        <select className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        {...register("branchId")}>
                                            <option value="regular">Regular</option>
                                            <option value="syariah">Syariah</option>
                                        </select>
                                            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                                <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="flex flex-wrap -mx-3 mb-6">
                                    <div className="w-full md:w-1/2 px-3">
                                        <div className="flex justify-end mt-3">
                                        <button 
                                        className="bg-blue-451 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                                        type="submit">
                                            Lanjut
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </Container>
                </Layout>
            </StickyProvider>
        </ThemeProvider>
    )
}

const styles = {
    container: {
        minHeight: 'inherit',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    card: {
        margin: 25
    },
    title: {
        color: "#FFFFFF"
    }, banner: {
        pt: ['140px', '145px', '155px', '170px', null, null, '180px', '215px'],
        pb: [2, null, 0, null, 2, 0, null, 5],
        position: 'relative',
        zIndex: 2,
        '&::before': {
            position: 'absolute',
            content: '""',
            bottom: 6,
            left: 0,
            height: '100%',
            width: '100%',
            zIndex: -1,
            backgroundRepeat: `no-repeat`,
            backgroundPosition: 'bottom left',
            backgroundSize: '36%',
        },
        '&::after': {
            position: 'absolute',
            content: '""',
            bottom: '40px',
            right: 0,
            height: '100%',
            width: '100%',
            zIndex: -1,
            backgroundRepeat: `no-repeat`,
            backgroundPosition: 'bottom right',
            backgroundSize: '32%',
        },
        container: {
            minHeight: 'inherit',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
        },
        contentBox: {
            width: ['100%', '90%', '535px', null, '57%', '60%', '68%', '60%'],
            mx: 'auto',
            textAlign: 'center',
            mb: ['40px', null, null, null, null, 7],
        },
        imageBox: {
            justifyContent: 'center',
            textAlign: 'center',
            display: 'inline-flex',
            mb: [0, null, -6, null, null, '10px', null, -3],
            img: {
                position: 'relative',
                height: [1000],
            },
        },
    },
}